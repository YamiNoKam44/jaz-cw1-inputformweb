<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html >
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <meta charset="UTF-8">
    <title>Loan Calc</title>
    </head>
<body>
    <div class='container'>

    <header>Loan Calc</header>
    <form action="calculator" method="post">
        <label for="amount">Kwota kredytu:</label> <input type="text" name="amount" /><br/>
        <label for="rateCount">Liczba rat:</label> <input type="text" name="rateCount" /><br/>
        <label for="interestRate">Opracowanie:</label> <input type="text" name="interestRate" /> %<br/>
        <label for="fixedFee">Opłata stała:</label> <input type="text" name="fixedFee" /><br/>
        <label for="rateType">Rodzaj raty:</label>
            <input type="radio" name="rateType" value="decreasing">malejąca
            <input type="radio" name="rateType" value="fixed">stała
            <input type="submit" value="Wyslij">
        </form>
    </div>
</body>
</html>