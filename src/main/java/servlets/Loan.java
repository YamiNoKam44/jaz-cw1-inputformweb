package servlets;

import java.io.IOException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


//metoda POST
@WebServlet("/calculator")
public class Loan extends HttpServlet{
	
	private static final long serialVersionUID = 1L;
	
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		
		String amount = req.getParameter("amount");
		String rateCount = req.getParameter("rateCount");
		String interestRate = req.getParameter("interestRate");
		String fixedFee = req.getParameter("fixedFee");
		String rateType = req.getParameter("rateType");
		double loanAmount = Double.parseDouble(amount);
		int loanRateCount = Integer.parseInt(rateCount);
		double loanInterestRate = Double.parseDouble(interestRate);
		double loanFixedFee = Double.parseDouble(fixedFee);
		if(amount == null || amount.equals("") && 
			rateCount == null || rateCount.equals("") &&
			interestRate == null || interestRate.equals("") &&
			fixedFee == null || fixedFee.equals("") &&
			rateType == null || rateType.equals("")) {
		resp.sendRedirect("/");
		}
		double loanArray[] = {loanAmount,loanInterestRate,loanFixedFee}; //poniewa� wysy�anie 5 parametr�w nie jest zbyt dobr� praktyk�...
		generateHtml(resp);
		loanCalculate(loanArray,loanRateCount, resp);
		resp.getWriter().println("</table>"+
		"</div> \n" +
		"</body> \n" +
		"</html>" );
		
	}
	
	private void loanCalculate(double[] arrayParam,int loanRateCount, HttpServletResponse resp) throws IOException{
		double interest = arrayParam[1]*0.01/12; // oprocentowanie w skali roku (podzia� na 12 miesi�cy)
		double capital = Math.round(arrayParam[0]/loanRateCount); // kredytu dzielony na ilosc miesiecy
		double interestAmount = Math.round(interest*arrayParam[0]); //odsetki
		double rate = Math.round(capital+interestAmount); // rata 
		int i;
		for (i=1; i<loanRateCount+1; i++){
			resp.getWriter().println(
					"<tr><td>" +i +
					"</td><td> " + capital +
					"</td><td> " + interestAmount +
					"</td><td>" + arrayParam[2] + 
					"</td><td> " + rate +
					"</td></tr>" );
			arrayParam[0] = Math.round(arrayParam[0]-capital);
			interestAmount = Math.round(interest*arrayParam[0]);
			rate = Math.round(capital+interestAmount);
		}
	}
	private void generateHtml(HttpServletResponse resp)  throws IOException {
		resp.setContentType("text/html");
		resp.getWriter().println("<!DOCTYPE HTML> \n" +
		"<html lang = 'pl' > \n" +
		"<head>\n" +
		"	<meta charset='utf-8'> \n" +
		" 	<title>Loan Calc</title> \n " +
		"</head> \n" +
		"<body> \n" +
		"<div class = 'container'> \n" +
		"<header> \n" +
		"Loan Calc \n" +
		"</header> \n" +
		" \n" +
		"<h1> Harmonogram sp�at:</h1>");
		resp.getWriter().println("<table border=4><tr><td>Nr raty </td>" + 
		"<td> Kwota kapita�u </td>" + 
		"<td> Kwota odsetek </td>" + 
		"<td> Op�aty sta�e </td>" + 
		"<td> Ca�kowita kwota raty </td></tr>");
	}
}